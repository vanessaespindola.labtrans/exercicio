# -*- coding: utf-8 -*-

def abrir_exibir_arquivo(nome_arq):
    '''
        abertura do arquivo txt e 
        exibição do seu conteúdo.
    '''
    arquivo = open(nome_arq, encoding="utf-8")
    todas_linhas = arquivo.readlines()
    for linha_da_vez in todas_linhas:
        print(linha_da_vez)

#abrir_exibir_arquivo("meu_arquivo.txt")

'''uma função para salvamento dos dados 
    inseridos em um arquivo .txt'''

def salvar_inserir_dados_em_arquivo(nome_arq, todos_os_dados_lista):
    arquivo = open(nome_arq, 'w+', encoding="utf-8")  
    for dado_da_vez in todos_os_dados_lista:
        arquivo.write(dado_da_vez)
        arquivo.write("\n")


# salvar_inserir_dados_em_arquivo('meu_arquivo.txt', lista)
